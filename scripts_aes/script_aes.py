from deep_learning_models.deeplearning import DeepLearningModel
from commons.sca_deep import ScaDeep
from commons.sca_plots import Plots
import h5py

sca_deep = ScaDeep()

#  define target
sca_deep.target("ches_ctf")

#  define leakage model
sca_deep.aes_leakage_model(leakage_model="HW", state_output="SubBytesOut")

#  define model
sca_deep.keras_model(DeepLearningModel().basic_mlp_ches_ctf, mlp=True)

# define hyper-parameters for random search for mlp
# sca_deep.random_search_mlp(
#     mini_batch=[200, 1000, 100],
#     learning_rate=[0.0001, 0.001],
#     activation=['relu', 'tanh', 'elu', 'selu'],
#     layers=[2, 8, 1],
#     neurons=[200, 600, 100]
# )

# define hyper-parameters for random search for cnn
# sca_deep.random_search_cnn(
#     mini_batch=[200, 1000, 100],
#     learning_rate=[0.0001, 0.001],
#     activation=['relu', 'elu', 'selu'],
#     conv_layers=[1, 1, 1],
#     filters=[8, 32, 4],
#     kernel_size=[10, 20, 2],
#     stride=[5, 10, 5],
#     dense_layers=[2, 3, 1],
#     neurons=[500, 1000, 100]
# )

#  train/validate and test
key_ranks = sca_deep.train(
    target_key_bytes=[0],
    early_stopping=True,
    ensemble=[50, 10],
    guessing_entropy=[10],
    success_rate=[10],
    # data_augmentation=True,
    multiple_trainings=[10]
)

# hf = h5py.File('output_probabilities_ascad_mlp_single.h5', 'w')
# hf.create_dataset('output_probabilities', data=sca_deep.get_output_probabilities())

#  plots
# Plots().plot_train(history, key_rank)
