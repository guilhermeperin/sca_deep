import plotly
import plotly.graph_objs as go
import plotly.figure_factory as ff
import pandas as pd
import numpy as np
import json
import scipy.stats


class PlotlyPlots:

    def set_color(self, x):
        if x < 16:
            return '#34eb77'
        elif x < 32:
            return '#4ceb34'
        elif x < 64:
            return '#89eb34'
        elif x < 96:
            return '#c0eb34'
        elif x < 128:
            return '#ebeb34'
        elif x < 160:
            return '#ebc934'
        elif x < 196:
            return '#eba234'
        elif x < 224:
            return '#eb7434'
        else:
            return '#eb3434'

    def create_line_plot(self, x=None, y=None, line_name="line_plot"):
        if x is None:
            x = np.linspace(1, len(y), len(y))

        df = pd.DataFrame({'x': x, 'y': y})  # creating a sample dataframe
        data = [
            go.Line(
                x=df['x'],  # assign x as the dataframe column 'x'
                y=df['y'],
                name=line_name,
                line={
                    'width': 1.5
                }
            )
        ]
        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def create_scatter_plot(self, x=None, y=None, z=None, line_name="line_plot"):
        if x is None:
            x = np.linspace(1, len(y), len(y))

        df = pd.DataFrame({'x': x, 'y': y})
        data = [
            go.Scatter(
                type="scatter",
                x=df['x'],
                y=df['y'],
                name=line_name,
                mode='markers',
                marker=dict(size=10, color=list(map(self.set_color, df['x'])))
            )
        ]
        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def create_scatter_plot_basic(self, x=None, y=None, line_name="line_plot"):
        if x is None:
            x = np.linspace(1, len(y), len(y))

        df = pd.DataFrame({'x': x, 'y': y})
        data = [
            go.Scatter(
                type="scatter",
                x=df['x'],
                y=df['y'],
                name=line_name,
                mode='markers',
                marker=dict(size=10)
            )
        ]
        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def create_scatter_hist_plot(self, x=None, y=None, z=None, line_name="line_plot"):
        df = pd.DataFrame({'x': x, 'y': y})

        trace1 = go.Scatter(
            type="scatter",
            x=df['x'],
            y=df['y'],
            name=line_name,
            mode='markers',
            marker=dict(size=10, color=list(map(self.set_color, df['x'])))
        )
        trace2 = go.Histogram2dcontour(
            x=x, y=y, name='density', ncontours=20,
            colorscale='Hot', reversescale=True, showscale=False
        )
        trace3 = go.Histogram(
            x=x, name='x density',
            marker=dict(color='#eb7434'),
            yaxis='y2'
        )
        trace4 = go.Histogram(
            y=y, name='y density', marker=dict(color='#eb7434'),
            xaxis='x2'
        )
        data = [trace1, trace3, trace4]

        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def create_hist_plot(self, x=None, line_name="line_plot"):

        x_bar = np.zeros(int(max(x)) + 1)

        for i in range(len(x)):
            x_bar[int(x[i])] += 1

        data = [go.Histogram(
            x=x,
            opacity=0.75,
            histnorm='probability',
            name=line_name,
        )]

        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def create_density_plot(self, x=None, line_name="line_plot"):

        l = np.linspace(0, max(x), len(x))

        line_data = go.Scatter(
            x=np.linspace(0, max(x), len(x)),
            y=scipy.stats.norm.pdf(l, np.mean(x), np.std(x)),
            fill='tozeroy',
            line_color='lightblue',
            name=line_name
        )

        # hist_data = go.Histogram(
        #     x=x,
        #     opacity=0.75,
        #     histnorm='probability',
        #     name=line_name,
        #     nbinsx=len(x)
        # )

        data = [line_data]

        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)


    def get_plotly_layout(self, x_axis_title, y_axis_title, color):
        layout = {
            "annotations": [],
            "font": {
                "size": 14,
                "family": 'Calibri',
                "color": '#fff'
            },
            # "width": 800,
            "paper_bgcolor": '#263238',
            "plot_bgcolor": '#263238',
            "xaxis": {
                "ticks": '',
                "side": 'bottom',
                "title": x_axis_title,
                "tickcolor": '#fff',
                "gridcolor": '#616161',
                "color": 'fff'
            },
            "yaxis": {
                "ticks": '',
                "ticksuffix": ' ',
                "autosize": False,
                "title": y_axis_title,
                "tickcolor": '#fff',
                "gridcolor": '#616161',
                "color": 'fff'
            }
        }

        if color == "lightgrey":
            layout['paper_bgcolor'] = '#fafafa'
            layout['plot_bgcolor'] = '#fafafa'
            layout['font']['color'] = '#263238'
            layout['xaxis']['color'] = '#263238'
            layout['xaxis']['gridcolor'] = '#d0d0d0'
            layout['yaxis']['color'] = '#263238'
            layout['yaxis']['gridcolor'] = '#d0d0d0'

        return layout

    def get_layout_density(self, x_axis_title, y_axis_title, color='dark'):
        layout = {
            "font": {
                "size": 12,
                "family": 'Calibri',
                "color": '#fff'
            },
            "showlegend": True,
            "autosize": True,
            "height": 550,
            "paper_bgcolor": '#263238',
            "plot_bgcolor": '#263238',
            'shapes': [],
            "xaxis": {
                "domain": [0, 1],
                "showgrid": False,
                "zeroline": False,
                "title": x_axis_title,
                "ticks": '',
                "side": 'bottom',
                "tickcolor": '#fff',
                "gridcolor": '#616161',
                "color": 'fff'
            },
            "yaxis": {
                "domain": [0, 1],
                "showgrid": False,
                "zeroline": False,
                "title": y_axis_title,
                "ticks": '',
                "side": 'bottom',
                "tickcolor": '#fff',
                "gridcolor": '#616161',
                "color": 'fff'
            },
            "margin": {
                "": 1
            },
            "hovermode": "closest",
            "bargap": 0.2,
            "xaxis2": {
                "domain": [0.85, 1],
                "showgrid": False,
                "zeroline": False
            },
            "yaxis2": {
                "domain": [0.85, 1],
                "showgrid": False,
                "zeroline": False
            }
        }

        if color == "lightgrey":
            layout['paper_bgcolor'] = '#fafafa'
            layout['plot_bgcolor'] = '#fafafa'
            layout['font']['color'] = '#263238'
            layout['xaxis']['color'] = '#263238'
            layout['xaxis']['gridcolor'] = '#d0d0d0'
            layout['yaxis']['color'] = '#263238'
            layout['yaxis']['gridcolor'] = '#d0d0d0'

        return layout

    def get_layout_histogram(self, x_axis_title, y_axis_title, color):
        layout = {
            "bargap": 0.2,
            "xaxis": {
                "title": x_axis_title,
            },
            "yaxis": {
                "title": y_axis_title
            }
        }
        return layout
