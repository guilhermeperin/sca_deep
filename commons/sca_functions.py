import numpy as np
import random
from crypto.aes import AES
from sklearn.utils import shuffle


class ScaFunctions:

    def success_rate(self, runs, key_rankings, nt):
        success_rate = np.zeros(nt)

        for index in range(nt):
            for i in range(runs):
                if key_rankings[i][index] == 1:
                    success_rate[index] += 1

        success_rate /= runs

        return success_rate

    def guessing_entropy(self, runs, model, param, aes_leakage_model, x_test, y_test):

        nt = len(x_test)
        key_ranking_sum = np.zeros(nt)
        key_rankings = np.zeros((runs, nt))
        key_probabilities_key_ranks = np.zeros((runs, nt, param["number_of_key_hypothesis"]))

        # ---------------------------------------------------------------------------------------------------------#
        # compute labels for key hypothesis
        # ---------------------------------------------------------------------------------------------------------#
        crypto = AES()
        labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], nt))
        for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
            key_h = bytearray.fromhex(param["key"])
            key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
            crypto.set_key(key_h)
            labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(y_test,
                                                                                 aes_leakage_model,
                                                                                 param,
                                                                                 key_hypothesis=True)

        # ---------------------------------------------------------------------------------------------------------#
        # predict output probabilities for shuffled test or validation set
        # ---------------------------------------------------------------------------------------------------------#
        output_probabilities = model.predict(x_test)

        # -------------------------------------------------------------------------------------------------------------#
        # compute key rank n times, to obtain guessing entropy
        # -------------------------------------------------------------------------------------------------------------#
        for run in range(runs):

            # ---------------------------------------------------------------------------------------------------------#
            # shuffle test or validation set
            # ---------------------------------------------------------------------------------------------------------#
            output_probabilities_shuffled, index_shuffled = shuffle(output_probabilities, list(range(0, nt)),
                                                                    random_state=random.randint(0, 10000))

            key_probabilities = np.zeros(param["number_of_key_hypothesis"])

            # ---------------------------------------------------------------------------------------------------------#
            # key probabilities for 100 key ranks
            # ---------------------------------------------------------------------------------------------------------#
            for index in range(nt):
                probabilities_kg = output_probabilities_shuffled[index][
                    np.asarray([int(leakage[index_shuffled[index]]) for leakage in labels_key_hypothesis[:]])
                ]

                key_probabilities += np.log(probabilities_kg + 1e-7)
                key_probabilities_sorted = np.argsort(key_probabilities)[::-1]

                key_probabilities_key_ranks[run][index] = probabilities_kg
                key_ranking_sum[index] += list(key_probabilities_sorted).index(param["good_key"]) + 1
                key_rankings[run][index] = list(key_probabilities_sorted).index(param["good_key"]) + 1

            print("Key rank ... {} | final guessing entropy for correct key ({}): {}".format(run,
                                                                                             param["good_key"],
                                                                                             key_ranking_sum[
                                                                                                 nt - 1] / (
                                                                                                     run + 1)))

        guessing_entropy = key_ranking_sum / runs
        # -------------------------------------------------------------------------------------------------------------#
        # return guessing entropy and sum of key probabilities
        # -------------------------------------------------------------------------------------------------------------#

        return guessing_entropy, key_rankings, key_probabilities_key_ranks, output_probabilities
