import matplotlib.pyplot as plt
from matplotlib import cycler
import matplotlib.colors as colors
import matplotlib.cm as cmx


class Plots:
    colors = cycler('color', ['#EE6666', '#3388BB', '#9988DD', '#EECC55', '#88BB44', '#FFBBBB'])
    plt.rc('axes', facecolor='#efefef', edgecolor='none', axisbelow=True, grid=True, prop_cycle=colors)
    plt.rc('grid', color='w', linestyle='solid')
    plt.rc('xtick', direction='out', color='black')
    plt.rc('ytick', direction='out', color='black')
    plt.rc('patch', edgecolor='#efefef')
    plt.rc('lines', linewidth=2)

    def __init__(self, rows=1, cols=1):
        self.rows = rows
        self.cols = cols

    def new_plot(self):
        plt.subplots(nrows=self.rows, ncols=self.cols)

    def create_line_plot(self, position_on_grid, list_of_series, x_label, y_label, show_legend=True, legend_out=False,
                         dashed=False):
        ax = plt.subplot(self.rows, self.cols, position_on_grid)
        for series in list_of_series:
            if series.get("legend"):
                plt.plot(series.get("data"), label=series.get("label"), color=series.get("color"), linewidth=1,
                         linestyle="--" if dashed else "-")
            else:
                plt.plot(series.get("data"), color=series.get("color"), linewidth=1, linestyle="--" if dashed else "-")
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        if show_legend:
            if legend_out:
                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
                plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            else:
                plt.legend()

    def create_scatter_plot(self, position_on_grid, list_of_series, x_label, y_label, show_legend=True):
        axis = plt.subplot(self.rows, self.cols, position_on_grid)
        for series in list_of_series:
            if series.get("legend"):
                plt.scatter(series.get("data_x"), series.get("data_y"), label=series.get("label"),
                            color=series.get("color"), edgecolor='black')
            else:
                plt.scatter(series.get("data_x"), series.get("data_y"), color=series.get("color"), edgecolor='black')
        plt.xlabel(x_label)
        plt.ylabel(y_label)

        # axis.set_ylim([-0.1, 0.8])
        # axis.set_xlim([-0.1, 8.1])

        if show_legend:
            plt.legend()

    def create_distribution_plot(self, position_on_grid, list_of_series, x_label, y_label, show_legend=True):
        plt.subplot(self.rows, self.cols, position_on_grid)
        for series in list_of_series:
            if series.get("legend"):
                plt.hist(series.get("data"), bins=30, label=series.get("label"), color=series.get("color"))
            else:
                plt.hist(series.get("data"), bins=30, color=series.get("color"))
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        if show_legend:
            plt.legend()

    def create_normal_distribution_plot(self, position_on_grid, list_of_series, x_label, y_label, show_legend=True):
        axis = plt.subplot(self.rows, self.cols, position_on_grid)

        for series in list_of_series:
            if series.get("legend"):
                plt.plot(series.get("data_x"), series.get("data_y"), label=series.get("label"),
                         color=series.get("color"))
            else:
                plt.plot(series.get("data_x"), series.get("data_y"), color=series.get("color"))

        plt.xlabel(x_label)
        plt.ylabel(y_label)

        if show_legend:
            plt.legend()

        plt.xlabel('x')
        plt.ylabel('Normal Distribution')

    @staticmethod
    def show_plot():
        plt.show()

    def plt(self):
        return plt

    @staticmethod
    def get_color_map():
        return plt.get_cmap('jet')

    def plot_train(self, history, key_rank):
        self.rows = 2
        self.cols = 2

        self.new_plot()

        list_of_series = [
            {"data": history.history['accuracy'], "label": "Training", "color": "blue", "legend": True},
            {"data": history.history['val_accuracy'], "label": "Validation", "color": "orange", "legend": True}
        ]
        self.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True)

        list_of_series = [
            {"data": history.history['recall'], "label": "Training", "color": "blue", "legend": True},
            {"data": history.history['val_recall'], "label": "Validation", "color": "orange", "legend": True}
        ]
        self.create_line_plot(2, list_of_series, "Epochs", "Recall", show_legend=True)

        list_of_series = [
            {"data": history.history['loss'], "label": "Training", "color": "blue", "legend": True},
            {"data": history.history['val_loss'], "label": "Validation", "color": "orange", "legend": True}
        ]
        self.create_line_plot(3, list_of_series, "Epochs", "Loss", show_legend=True)

        list_of_series = [
            {"data": key_rank, "label": "Correct Key", "color": "blue", "legend": True}
        ]
        self.create_line_plot(4, list_of_series, "Number of Traces", "Key Rank", show_legend=True)

        self.show_plot()
