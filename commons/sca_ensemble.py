import numpy as np


class ScaEnsemble:

    def __init__(self):
        pass

    def get_best_models(self, n_models, n_best, result_models_validation, n_traces):

        result_number_of_traces_val = []
        for model_index in range(n_models):
            if result_models_validation[model_index][n_traces - 1] == 1:
                for index in range(n_traces - 1, -1, -1):
                    if result_models_validation[model_index][index] != 1:
                        result_number_of_traces_val.append(
                            [result_models_validation[model_index][n_traces - 1], index + 1,
                             model_index])
                        break
            else:
                result_number_of_traces_val.append(
                    [result_models_validation[model_index][n_traces - 1], n_traces,
                     model_index])

        sorted_models = sorted(result_number_of_traces_val, key=lambda l: l[:])

        list_of_best_models = []
        for model_index in range(n_best):
            list_of_best_models.append(sorted_models[model_index][2])

        return list_of_best_models
