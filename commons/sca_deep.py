import numpy as np
import pandas as pd
import time
import random
import os
from keras import backend
from keras.utils import to_categorical
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_datasets import ScaDataSets
from commons.sca_callbacks import ValidationCallback, CalculateITY, TestCallback
from commons.sca_keras_models import ScaKerasModels
from commons.sca_ensemble import ScaEnsemble
from commons.sca_functions import ScaFunctions
from commons.sca_data_augmentation import data_augmentation_shifts, data_augmentation_noise
from database.database_inserts import DatabaseInserts
from deep_learning_models.deeplearning import DeepLearningModel


class ScaDeep:

    def __init__(self):
        self.target_trace_set = None
        self.directory = 'D:/traces/'
        self.sca_parameters = ScaParameters()
        self.target_params = None
        self.aes_lm = None
        self.crypto = AES()
        self.callbacks = []
        self.model = None
        self.random_model = None
        self.model_obj = None
        self.model_name = None
        self.db_inserts = None

        self.metric_names = None
        self.early_stopping_metrics = None
        self.n_es_metrics = None
        self.n_metrics = None

        self.ge_test = None
        self.ge_val = None
        self.sr_test = None
        self.sr_val = None
        self.key_ranks_test = None
        self.key_ranks_val = None
        self.key_ps_test = None
        self.output_probabilities = None

        self.hyper_parameters = []
        self.learning_rate = None
        self.optimizer = None

        self.random_search_hyper_parameters = False
        self.grid_search_hyper_parameters = False
        self.mlp = None
        self.cnn = None
        self.mini_batch_random = None
        self.learning_rate_random = None
        self.activation_random = None
        self.conv_layers_random = None
        self.filters_random = None
        self.kernel_size_random = None
        self.stride_random = None
        self.layers_random = None
        self.neurons_random = None
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    def directory(self, directory):
        self.directory = directory

    def target(self, target):
        self.target_trace_set = target
        self.sca_parameters = ScaParameters()
        self.target_params = self.sca_parameters.get_trace_set(self.target_trace_set)

    def param(self):
        if self.target_params is not None:
            return self.target_params
        else:
            print("Parameters (param) from target is not selected. Set target before retrieving parameters.")
            return None

    def sca_parameters(self):
        return self.sca_parameters

    def aes_leakage_model(self, leakage_model="HW", bit=0, byte=0, state_output="SubBytesOut", operation="encryption",
                          direction="input"):
        self.aes_lm = {
            "round_state_input": 1,
            "round_state_output": 1,
            "state_input": "",
            "state_output": state_output,
            "leakage_model": leakage_model,
            "bit": bit,
            "byte": byte,
            "operation": operation,
            "direction": direction
        }

        if self.target_params is not None:
            if self.aes_lm["leakage_model"] == "HW":
                self.target_params["classes"] = 9
            elif self.aes_lm["leakage_model"] == "ID":
                self.target_params["classes"] = 256
            else:
                self.target_params["classes"] = 2
        else:
            print("Parameters (param) from target is not selected. Set target before the leakage model.")

        return self.aes_lm

    def train_validation_sets(self):
        if self.target_params is not None:
            data_sets = ScaDataSets(self.target_params, self.directory)
            x_train, x_validation, train_data, validation_data, train_samples, validation_samples, _, _ = \
                data_sets.load_training_and_validation_sets()
            return x_train, x_validation, train_samples, validation_samples, train_data, validation_data
        else:
            print("Parameters (param) from target is not selected. Set target before retrieving the data sets.")

    def test_set(self):
        if self.target_params is not None:
            data_sets = ScaDataSets(self.target_params, self.directory)
            x_test, test_data, test_samples = data_sets.load_test_set()
            return x_test, test_samples, test_data
        else:
            print("Parameters (param) from target is not selected. Set target before retrieving the data sets.")
            return None

    def test_sets(self):
        if self.target_params is not None:
            data_sets = ScaDataSets(self.target_params, self.directory)
            x_test_1, x_test_2, test_data_1, test_data_2, test_samples_1, test_samples_2 = \
                data_sets.load_two_test_sets()
            return x_test_1, test_samples_1, test_data_1, x_test_2, test_samples_2, test_data_2
        else:
            print("Parameters (param) from target is not selected. Set target before retrieving the data sets.")

    def add_callback(self, callback):
        self.callbacks.append(callback)
        return self.callbacks

    def clear_callbacks(self):
        self.callbacks = []

    def keras_model(self, model, mlp=False, cnn=False):
        self.model_name = model
        self.model_obj = model
        self.model = model(self.target_params["classes"], self.target_params["number_of_samples"])
        self.mlp = mlp
        self.cnn = cnn

    def get_model(self):
        return self.model

    def get_test_guessing_entropy(self):
        return self.ge_test

    def get_val_guessing_entropy(self):
        return self.ge_val

    def get_test_success_rate(self):
        return self.sr_test

    def get_val_success_rate(self):
        return self.sr_val

    def get_key_prob_test(self):
        return self.key_ps_test

    def get_output_probabilities(self):
        return self.output_probabilities

    def __set_metrics(self, early_stopping):
        if self.random_search_hyper_parameters:
            self.random_model = self.__keras_model_random()
            self.model = self.random_model["model"]
        self.metric_names = self.model.metrics_names
        self.n_metrics = len(self.metric_names)
        if early_stopping:
            self.early_stopping_metrics = []
            for metric in self.metric_names:
                self.early_stopping_metrics.append(metric)
            self.early_stopping_metrics.append("ity")
            self.early_stopping_metrics.append("all_epochs")
            self.n_es_metrics = len(self.early_stopping_metrics)
        else:
            self.n_es_metrics = 1

    def random_search_mlp(self, mini_batch=None, learning_rate=None, activation=None, layers=None, neurons=None):
        self.random_search_hyper_parameters = True
        if self.grid_search_hyper_parameters:
            self.grid_search_hyper_parameters = False

        self.mini_batch_random = mini_batch
        self.learning_rate_random = learning_rate
        self.activation_random = activation
        self.layers_random = layers
        self.neurons_random = neurons
        self.mlp = True
        self.cnn = False

    def random_search_cnn(self, mini_batch=None, learning_rate=None, activation=None, conv_layers=None, filters=None,
                          kernel_size=None, stride=None, dense_layers=None, neurons=None):
        self.random_search_hyper_parameters = True
        if self.grid_search_hyper_parameters:
            self.grid_search_hyper_parameters = False

        self.mini_batch_random = mini_batch
        self.learning_rate_random = learning_rate
        self.activation_random = activation
        self.conv_layers_random = conv_layers
        self.filters_random = filters
        self.kernel_size_random = kernel_size
        self.stride_random = stride
        self.layers_random = dense_layers
        self.neurons_random = neurons
        self.mlp = False
        self.cnn = True

    def grid_search(self, mini_batch=None, learning_rate=None, activation=None, layers=None, neurons=None):
        self.grid_search_hyper_parameters = True
        if self.random_search_hyper_parameters:
            self.random_search_hyper_parameters = False

        self.mini_batch_random = mini_batch
        self.learning_rate_random = learning_rate
        self.activation_random = activation
        self.layers_random = layers
        self.neurons_random = neurons

    def __keras_model_random(self):
        # cnn and mlp
        mini_batch = self.__get_random_range(self.mini_batch_random)
        neurons = self.__get_random_range(self.neurons_random)
        layers = self.__get_random_range(self.layers_random)
        learning_rate = self.__get_random_uniform(self.learning_rate_random)
        activation = random.choice(self.activation_random)

        # cnn
        conv_layers = self.__get_random_range(self.conv_layers_random) if self.cnn else 0
        filters = self.__get_random_range(self.filters_random) if self.cnn else 0
        kernel_size = self.__get_random_range(self.kernel_size_random) if self.cnn else 0
        stride = self.__get_random_range(self.stride_random) if self.cnn else 0

        if self.mlp:
            random_model_obj = DeepLearningModel().basic_mlp_random
            random_model = random_model_obj(
                self.target_params["classes"],
                self.target_params["number_of_samples"],
                activation, neurons, layers, learning_rate
            )
        else:
            random_model_obj = DeepLearningModel().basic_cnn_random
            random_model = random_model_obj(
                self.target_params["classes"],
                self.target_params["number_of_samples"],
                activation, neurons, conv_layers, filters, kernel_size, stride, layers, learning_rate
            )

        self.target_params["mini-batch"] = mini_batch

        return {
            "model_obj": random_model_obj,
            "model": random_model,
            "mini_batch": mini_batch,
            "activation": activation,
            "filters": filters if self.cnn else 0,
            "kernel_size": kernel_size if self.cnn else 0,
            "stride": stride if self.cnn else 0,
            "neurons": neurons,
            "layers": layers,
            "conv_layers": conv_layers if self.cnn else 0,
            "learning_rate": learning_rate
        }

    def train(self, target_key_bytes=None, early_stopping=False, data_augmentation=False, guessing_entropy=None,
              success_rate=None, ensemble=None, multiple_trainings=None):

        if ensemble is not None and multiple_trainings is not None:
            print("Cannot run ensemble and multiple trainings together.")
            return

        self.__set_metrics(early_stopping)

        ensemble_active = False
        number_of_models = 1
        number_of_best_models = 1
        if ensemble is not None:
            ensemble_active = True
            number_of_models = ensemble[0]
            number_of_best_models = ensemble[1]

        if multiple_trainings is not None:
            number_of_models = multiple_trainings[0]

        ge_runs = 1
        if guessing_entropy is not None:
            ge_runs = guessing_entropy[0]

        success_rate_active = False
        if success_rate is not None:
            success_rate_active = True
            sr_runs = success_rate[0]
            if guessing_entropy is None:
                ge_runs = sr_runs

        key_int = ([int(x) for x in bytearray.fromhex(self.target_params["key"])])
        if target_key_bytes is None:
            target_key_bytes = list(range(0, len(key_int)))

        x_train, x_val, train_samples, val_samples, train_data, validation_data = self.train_validation_sets()
        x_test_1, test_samples_1, test_data_1 = self.test_set()

        test_samples_2 = None
        test_data_2 = None
        x_test_2 = None
        if ensemble_active:
            x_test_1, test_samples_1, test_data_1, x_test_2, test_samples_2, test_data_2 = self.test_sets()

        x_t = x_train
        x_v = x_val
        x_t1 = x_test_1
        x_t2 = x_test_2
        if self.mlp:
            x_t = train_samples
            x_v = val_samples
            x_t1 = test_samples_1
            x_t2 = test_samples_2

        first_key_byte = True

        nt_test = len(test_samples_1)
        if ensemble_active:
            nt_test = len(test_samples_2)

        model_name = None
        history = None

        # it is + 1 because we also add "ity" as a validation metric (to be fixed later)
        metric_training = np.zeros((self.n_metrics, number_of_models, self.target_params["epochs"]))
        metric_validation = np.zeros((self.n_metrics + 1, number_of_models, self.target_params["epochs"]))

        self.ge_test = np.zeros((self.n_es_metrics, number_of_models, len(test_samples_1)))
        self.ge_val = np.zeros((self.n_es_metrics, number_of_models, nt_test))

        self.sr_test = np.zeros((self.n_es_metrics, number_of_models, len(test_samples_1)))
        self.sr_val = np.zeros((self.n_es_metrics, number_of_models, nt_test))

        self.key_ranks_test = np.zeros((self.n_es_metrics, number_of_models, ge_runs, len(test_samples_1)))
        self.key_ranks_val = np.zeros((self.n_es_metrics, number_of_models, ge_runs, nt_test))

        self.key_ps_test = np.zeros((self.n_es_metrics, number_of_models, ge_runs, len(test_samples_1),
                                     self.target_params["number_of_key_hypothesis"]))

        self.output_probabilities = np.zeros((self.n_es_metrics, number_of_models, nt_test,
                                              self.target_params["classes"]))

        start = time.time()

        test_acc = None

        for key_byte_index in target_key_bytes:

            for model_index in range(number_of_models):

                self.target_params["good_key"] = key_int[key_byte_index]
                self.aes_lm["byte"] = key_byte_index

                train_labels = self.crypto.create_labels(train_data, self.aes_lm, self.target_params)
                validation_labels = self.crypto.create_labels(validation_data, self.aes_lm, self.target_params)

                y_train = to_categorical(train_labels, num_classes=self.target_params["classes"])
                y_val = to_categorical(validation_labels, num_classes=self.target_params["classes"])

                self.clear_callbacks()
                callback_mia_validation_ity = None

                callbacks = []
                if early_stopping:
                    callbacks_validation = ValidationCallback(x_v, y_val, save_model=True)
                    callback_mia_validation_ity = CalculateITY(x_v, y_val, 100, save_model=True)
                    self.add_callback(callbacks_validation)
                    self.add_callback(callback_mia_validation_ity)

                test_labels = self.crypto.create_labels(test_data_1, self.aes_lm, self.target_params)
                y_test = to_categorical(test_labels, num_classes=self.target_params["classes"])
                test_callback = TestCallback(x_t1, y_test)
                callbacks = self.add_callback(test_callback)

                print("\nKey Byte {} | Model Index {}\n".format(key_byte_index, model_index))

                os.environ["CUDA_VISIBLE_DEVICES"] = "0"
                if self.random_search_hyper_parameters:
                    self.random_model = self.__keras_model_random()
                    self.model = self.random_model["model"]
                    self.model_obj = self.random_model["model_obj"]
                else:
                    self.model = self.model_obj(self.target_params["classes"], self.target_params["number_of_samples"])
                if data_augmentation:
                    history = self.model.fit_generator(
                        generator=data_augmentation_noise(train_samples, y_train, self.target_params, self.mlp),
                        steps_per_epoch=500,
                        epochs=self.target_params["epochs"],
                        validation_data=data_augmentation_noise(val_samples, y_val, self.target_params, self.mlp),
                        validation_steps=1,
                        callbacks=callbacks)
                else:
                    history = self.model.fit(
                        x=x_t,
                        y=y_train,
                        batch_size=self.target_params["mini-batch"],
                        verbose=1,
                        epochs=self.target_params["epochs"],
                        shuffle=True,
                        validation_data=(x_v, y_val),
                        callbacks=callbacks)

                os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

                model_name = self.model_obj.__name__
                self.learning_rate = backend.eval(self.model.optimizer.lr)
                self.optimizer = self.model.optimizer.__class__.__name__

                models = self.__get_models_metrics(early_stopping)
                for m_index in range(self.n_es_metrics):
                    self.ge_test[m_index][model_index], \
                    self.key_ranks_test[m_index][model_index], \
                    self.key_ps_test[m_index][model_index], \
                    self.output_probabilities[m_index][model_index] = \
                        ScaFunctions().guessing_entropy(ge_runs, models[m_index], self.target_params,
                                                        self.aes_lm, x_t1, test_data_1)

                    if ensemble_active:
                        self.ge_val[m_index][model_index], \
                        self.key_ranks_val[m_index][model_index], _, _ = \
                            ScaFunctions().guessing_entropy(ge_runs, models[m_index], self.target_params,
                                                            self.aes_lm, x_t2, test_data_2)

                if early_stopping:
                    ge = []
                    for m_index in range(self.n_es_metrics):
                        ge.append(self.ge_test[m_index][model_index][len(test_samples_1) - 1])
                    self.__set_hyper_parameters(ge)
                else:
                    self.__set_hyper_parameters(self.ge_test[0][model_index][len(test_samples_1) - 1])

                m_index = 0
                for metric in self.metric_names:
                    metric_training[m_index][model_index] = history.history[metric]
                    metric_validation[m_index][model_index] = history.history['val_' + metric]
                    m_index += 1

                if early_stopping:
                    metric_validation[m_index][model_index] = callback_mia_validation_ity.get_ity()

                test_acc = test_callback.get_accuracy()

                backend.clear_session()

            # ---------------------------------------------------------------------------------------------------------#
            #  Save results in database
            # ---------------------------------------------------------------------------------------------------------#
            elapsed_time = time.time() - start

            if first_key_byte:
                self.db_inserts = DatabaseInserts('database.sqlite', self.target_params["name"],
                                                  "script_single_all_keys.py", elapsed_time)

                leakage_model = [{
                    "cipher": "AES",
                    "model": self.aes_lm["leakage_model"],
                    "operation": self.aes_lm["state_output"],
                    "direction": self.aes_lm["direction"]
                }]

                sca_keras_model = ScaKerasModels()
                keras_model_description = sca_keras_model.keras_model_as_string(model_name)

                self.db_inserts.save_neural_network(keras_model_description, model_name)
                self.db_inserts.save_hyper_parameters(self.hyper_parameters)
                self.db_inserts.save_leakage_model(leakage_model)

                first_key_byte = False
            else:
                self.db_inserts.update_elapsed_time_analysis(elapsed_time)

            # -------------------------------------------------------------------------------------------------------- #
            # Get best models from 'validation key rank' or 'validation guessing entropy'
            # -------------------------------------------------------------------------------------------------------- #
            sorted_models = np.zeros((self.n_es_metrics, number_of_models))
            best_models = np.zeros((self.n_es_metrics, number_of_best_models))
            best_models_only_metrics = np.zeros((self.n_metrics + 1, number_of_best_models))  # plus 1 for ity
            ge_best_model_test = np.zeros((self.n_es_metrics, len(test_samples_1)))
            ge_best_model_validation = np.zeros((self.n_es_metrics, nt_test))

            for m_index in range(self.n_es_metrics):
                best_models[m_index] = ScaEnsemble().get_best_models(
                    number_of_models, number_of_best_models, self.ge_val[m_index], nt_test
                )
                sorted_models[m_index] = ScaEnsemble().get_best_models(
                    number_of_models, number_of_models, self.ge_val[m_index], nt_test
                )

            if early_stopping:
                for m_index in range(self.n_metrics):
                    best_models_only_metrics[m_index] = ScaEnsemble().get_best_models(
                        number_of_models, number_of_best_models, self.ge_val[m_index], nt_test
                    )
                # for ity
                best_models_only_metrics[self.n_metrics] = ScaEnsemble().get_best_models(
                    number_of_models, number_of_best_models, self.ge_val[self.n_metrics], nt_test)

            for m_index in range(self.n_es_metrics):
                ge_best_model_test[m_index] = self.ge_test[m_index][int(best_models[m_index][0])]

            for m_index in range(self.n_es_metrics):
                ge_best_model_validation[m_index] = self.ge_val[m_index][int(best_models[m_index][0])]

            # ---------------------------------------------------------------------------------------------------- #
            # Compute average metrics for ensembles
            # ---------------------------------------------------------------------------------------------------- #
            m_index = 0
            for metric in self.metric_names:
                if ensemble_active:
                    self.__save_metric_avg(metric_training[m_index], number_of_models, metric + "_ensembles")
                self.__save_metric(metric_training[m_index][int(best_models_only_metrics[m_index][0])], metric)
                if metric == "accuracy":
                    self.__save_metric(test_acc, "test_" + metric)
                m_index += 1

            if ensemble_active:
                m_index = 0
                for metric in self.metric_names:
                    self.__save_metric_best_models(metric_training[m_index], best_models_only_metrics[m_index],
                                                   metric + "_ensembles_best_models")
                    m_index += 1

            if early_stopping:
                self.metric_names.append("ity")  # append custom metrics (from callbacks) like this
            m_index = 0
            for metric in self.metric_names:
                if ensemble_active:
                    self.__save_metric_avg(metric_validation[m_index], number_of_models, "val_" + metric + "_ensembles")
                self.__save_metric(metric_validation[m_index][int(best_models_only_metrics[m_index][0])],
                                   "val_" + metric)
                m_index += 1

            if ensemble_active:
                m_index = 0
                for metric in self.metric_names:
                    self.__save_metric_best_models(metric_validation[m_index], best_models_only_metrics[m_index],
                                                   "val_" + metric + "_ensembles_best_models")
                    m_index += 1

            # -------------------------------------------------------------------------------------------------------- #
            #  Compute key ranks for all metrics
            # ---------------------------------------------------------------------------------------------------------#
            if multiple_trainings is not None:
                ge_test_avg = np.zeros((self.n_es_metrics, len(test_samples_1)))
                for m_index in range(self.n_es_metrics):
                    for model_index in range(number_of_models):
                        ge_test_avg[m_index] += self.ge_test[m_index][model_index]
                    ge_test_avg[m_index] /= number_of_models
                    self.__save_kr(ge_test_avg[m_index], "Average GE " + self.early_stopping_metrics[m_index])

                sr_test_avg = np.zeros((self.n_es_metrics, len(test_samples_1)))
                for model_index in range(number_of_models):
                    for index in range(nt_test):
                        for run in range(ge_runs):
                            for m_index in range(self.n_es_metrics):
                                sr_test_avg[m_index][index] += self.__add_if_one(self.key_ranks_test[m_index][
                                                                                     model_index][run][index])
                for m_index in range(self.n_es_metrics):
                    sr_test_avg[m_index] /= number_of_models
                    self.__save_sr(sr_test_avg[m_index], "Average SR " + self.early_stopping_metrics[m_index])
            else:
                kr_ensemble = np.zeros((self.n_es_metrics, nt_test))
                krs_ensemble = np.zeros((self.n_es_metrics, ge_runs, nt_test))
                kr_ensemble_best_models = np.zeros((self.n_es_metrics, nt_test))
                krs_ensemble_best_models = np.zeros((self.n_es_metrics, ge_runs, nt_test))

                for run in range(ge_runs):

                    key_p_ensemble = np.zeros((self.n_es_metrics, self.target_params["number_of_key_hypothesis"]))
                    key_p_ensemble_best_models = np.zeros(
                        (self.n_es_metrics, self.target_params["number_of_key_hypothesis"]))

                    for index in range(len(test_samples_1)):
                        for m_index in range(self.n_es_metrics):
                            for model_index in range(number_of_models):
                                key_p_ensemble[m_index] += \
                                    np.log(self.key_ps_test[m_index][int(sorted_models[m_index][model_index])][run][
                                               index] + 1e-10)
                            if ensemble_active:
                                for model_index in range(number_of_best_models):
                                    key_p_ensemble_best_models[m_index] += \
                                        np.log(self.key_ps_test[m_index][int(best_models[m_index][model_index])][run][
                                                   index] + 1e-10)

                        key_p_ensemble_sorted = np.zeros(
                            (self.n_es_metrics, self.target_params["number_of_key_hypothesis"]))
                        for m_index in range(self.n_es_metrics):
                            key_p_ensemble_sorted[m_index] = np.argsort(key_p_ensemble[m_index])[::-1]

                        key_p_ensemble_best_models_sorted = np.zeros(
                            (self.n_es_metrics, self.target_params["number_of_key_hypothesis"]))
                        if ensemble_active:
                            for m_index in range(self.n_es_metrics):
                                key_p_ensemble_best_models_sorted[m_index] = np.argsort(
                                    key_p_ensemble_best_models[m_index])[::-1]

                        for m_index in range(self.n_es_metrics):
                            kr_ensemble[m_index][index] += list(key_p_ensemble_sorted[m_index]).index(
                                self.target_params["good_key"]) + 1
                            krs_ensemble[m_index][run][index] = list(key_p_ensemble_sorted[m_index]).index(
                                self.target_params["good_key"]) + 1
                            if ensemble_active:
                                kr_ensemble_best_models[m_index][index] += list(
                                    key_p_ensemble_best_models_sorted[m_index]).index(
                                    self.target_params["good_key"]) + 1
                                krs_ensemble_best_models[m_index][run][index] = list(
                                    key_p_ensemble_best_models_sorted[m_index]).index(
                                    self.target_params["good_key"]) + 1

                    for m_index in range(self.n_es_metrics):
                        print(
                            "Run {} - GE {} models: {} | GE {} models: {} | GE single test model: {} | GE single val model: {}".format(
                                run,
                                number_of_models,
                                int(kr_ensemble[m_index][nt_test - 1] / (run + 1)),
                                number_of_best_models,
                                int(kr_ensemble_best_models[m_index][nt_test - 1] / (run + 1)),
                                ge_best_model_test[m_index][nt_test - 1],
                                ge_best_model_validation[m_index][nt_test - 1]
                            ))

                ge_ensemble = np.zeros((self.n_es_metrics, nt_test))
                ge_ensemble_best_models = np.zeros((self.n_es_metrics, nt_test))
                for m_index in range(self.n_es_metrics):
                    ge_ensemble[m_index] = kr_ensemble[m_index] / ge_runs
                    ge_ensemble_best_models[m_index] = kr_ensemble_best_models[m_index] / ge_runs

                sr_ensemble = np.zeros((self.n_es_metrics, nt_test))
                sr_ensemble_best_models = np.zeros((self.n_es_metrics, nt_test))

                sr_best_model_test = np.zeros((self.n_es_metrics, nt_test))
                sr_best_model_val = np.zeros((self.n_es_metrics, nt_test))

                for index in range(nt_test):
                    for run in range(ge_runs):
                        for m_index in range(self.n_es_metrics):
                            sr_ensemble[m_index][index] += self.__add_if_one(krs_ensemble[m_index][run][index])
                            if ensemble_active:
                                sr_ensemble_best_models[m_index][index] += self.__add_if_one(
                                    krs_ensemble_best_models[m_index][run][index])
                                sr_best_model_test[m_index][index] += self.__add_if_one(
                                    self.key_ranks_test[m_index][int(best_models[m_index][0])][run][index])
                                sr_best_model_val[m_index][index] += self.__add_if_one(
                                    self.key_ranks_val[m_index][int(best_models[m_index][0])][run][index])

                for m_index in range(self.n_es_metrics):
                    metric = " - " + self.early_stopping_metrics[m_index] if early_stopping else ""
                    kr_label = "Ensemble " + str(
                        number_of_models) + " Models - " + metric if ensemble_active else "Test " + metric

                    self.__save_kr(ge_ensemble[m_index], kr_label)
                    self.__save_sr(sr_ensemble[m_index] / ge_runs, kr_label)
                    if ensemble_active:
                        self.__save_kr(ge_ensemble_best_models[m_index],
                                       "Ensemble " + str(number_of_best_models) + " Best Models" + metric)
                        self.__save_kr(ge_best_model_test[m_index], "Best Model - Test" + metric)
                        self.__save_kr(ge_best_model_validation[m_index], "Best Model - Validation" + metric)
                        if success_rate_active:
                            self.__save_sr(sr_ensemble_best_models[m_index] / ge_runs,
                                           "Ensemble " + str(number_of_best_models) + " Best Models" + metric)
                            self.__save_sr(sr_best_model_test[m_index] / ge_runs, "Best Model - Test " + metric)
                            self.__save_sr(sr_best_model_val[m_index] / ge_runs, "Best Model - Validation" + metric)

                if ensemble_active:
                    final_ge_test = []
                    final_ge_val = []
                    for m_index in range(self.n_es_metrics):
                        for model_index in range(number_of_models):
                            final_ge_test.append(self.ge_test[m_index][model_index][len(test_samples_1) - 1])
                            final_ge_val.append(self.ge_val[m_index][model_index][nt_test - 1])
                    self.__save_final_ge(final_ge_test, final_ge_val)

    def __save_metric_avg(self, metric, n_models, name):
        kr_avg = sum(metric[n] for n in range(n_models)) / n_models
        self.db_inserts.save_metric(kr_avg, self.aes_lm["byte"], name)

    def __save_metric(self, metric, name):
        self.db_inserts.save_metric(metric, self.aes_lm["byte"], name)

    def __save_metric_best_models(self, metric, best_models, name):
        m_avg = sum(metric[int(best_models[n])] for n in range(len(best_models))) / len(best_models)
        self.db_inserts.save_metric(m_avg, self.aes_lm["byte"], name)

    def __save_kr(self, kr, name):
        self.db_inserts.save_key_rank_json(pd.Series(kr).to_json(), self.aes_lm["byte"], name)

    def __save_sr(self, sr, name):
        self.db_inserts.save_success_rate_json(pd.Series(sr).to_json(), self.aes_lm["byte"], name)

    def __save_final_ge(self, final_ge_test, final_ge_val):
        self.db_inserts.save_ensemble(pd.Series(final_ge_val).to_json(), self.aes_lm["byte"], "Validation")
        self.db_inserts.save_ensemble(pd.Series(final_ge_test).to_json(), self.aes_lm["byte"], "Test")

    @staticmethod
    def __add_if_one(value):
        return 1 if value == 1 else 0

    def __get_models_metrics(self, early_stopping):
        models = []
        if early_stopping:
            for metric in self.metric_names:
                model_metric = None
                if self.random_search_hyper_parameters:
                    if self.cnn:
                        model_metric = self.model_obj(self.target_params["classes"],
                                                      self.target_params["number_of_samples"],
                                                      self.random_model["activation"], self.random_model["neurons"],
                                                      self.random_model["conv_layers"], self.random_model["filters"],
                                                      self.random_model["kernel_size"], self.random_model["stride"],
                                                      self.random_model["layers"], self.random_model["learning_rate"])
                    else:
                        model_metric = self.model_obj(self.target_params["classes"],
                                                      self.target_params["number_of_samples"],
                                                      self.random_model["activation"], self.random_model["neurons"],
                                                      self.random_model["layers"], self.random_model["learning_rate"])
                else:
                    model_metric = self.model_obj(self.target_params["classes"],
                                                  self.target_params["number_of_samples"])
                model_metric.load_weights("best_model_" + metric + ".h5")
                models.append(model_metric)
            model_ity = None
            if self.random_search_hyper_parameters:
                if self.cnn:
                    model_ity = self.model_obj(self.target_params["classes"],
                                               self.target_params["number_of_samples"],
                                               self.random_model["activation"], self.random_model["neurons"],
                                               self.random_model["conv_layers"], self.random_model["filters"],
                                               self.random_model["kernel_size"], self.random_model["stride"],
                                               self.random_model["layers"], self.random_model["learning_rate"])
                else:
                    model_ity = self.model_obj(self.target_params["classes"],
                                               self.target_params["number_of_samples"],
                                               self.random_model["activation"], self.random_model["neurons"],
                                               self.random_model["layers"], self.random_model["learning_rate"])
            else:
                model_ity = self.model_obj(self.target_params["classes"],
                                           self.target_params["number_of_samples"])
            model_ity.load_weights("best_model_ity.h5")
            models.append(model_ity)
        models.append(self.model)
        return models

    @staticmethod
    def __get_random_range(values):
        if values[0] != values[1]:
            return random.randrange(values[0], values[1] + 1, values[2])
        else:
            return values[0]

    @staticmethod
    def __get_random_uniform(values):
        if values[0] != values[1]:
            return random.uniform(values[0], values[1])
        else:
            return values[0]

    def __set_hyper_parameters(self, key_rank):
        if self.random_search_hyper_parameters:
            if self.mlp:
                self.hyper_parameters.append({
                    "key_rank": key_rank,
                    "mini_batch": self.random_model["mini_batch"],
                    "epochs": self.target_params["epochs"],
                    "activation": self.random_model["activation"],
                    "neurons": self.random_model["neurons"],
                    "layers": self.random_model["layers"],
                    "learning_rate": float(round(self.random_model["learning_rate"], 6)),
                    "optimizer": str(self.optimizer),
                    "training_set": self.target_params["n_train"],
                    "validation_set": self.target_params["n_validation"],
                    "test_set": self.target_params["n_test"]
                })
            else:
                self.hyper_parameters.append({
                    "key_rank": key_rank,
                    "mini_batch": self.random_model["mini_batch"],
                    "epochs": self.target_params["epochs"],
                    "activation": self.random_model["activation"],
                    "neurons": self.random_model["neurons"],
                    "conv_layers": self.random_model["conv_layers"],
                    "filters": self.random_model["filters"],
                    "kernel_size": self.random_model["kernel_size"],
                    "stride": self.random_model["stride"],
                    "layers": self.random_model["layers"],
                    "learning_rate": float(round(self.random_model["learning_rate"], 6)),
                    "optimizer": str(self.optimizer),
                    "training_set": self.target_params["n_train"],
                    "validation_set": self.target_params["n_validation"],
                    "test_set": self.target_params["n_test"]
                })
        else:
            self.hyper_parameters.append({
                "key_rank": key_rank,
                "mini_batch": self.target_params["mini-batch"],
                "epochs": self.target_params["epochs"],
                "learning_rate": float(self.learning_rate),
                "optimizer": str(self.optimizer),
                "training_set": self.target_params["n_train"],
                "validation_set": self.target_params["n_validation"],
                "test_set": self.target_params["n_test"]
            })
