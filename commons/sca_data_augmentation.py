import random
import numpy as np


# ---------------------------------------------------------------------------------------------------------------------#
#  Functions for Data Augmentation
# ---------------------------------------------------------------------------------------------------------------------#
def data_augmentation_shifts(data_set_samples, data_set_labels, param, mlp=False):
    ns = param["number_of_samples"]

    while True:

        x_train_shifted = np.zeros((param["mini-batch"], ns))
        rnd = random.randint(0, len(data_set_samples) - param["mini-batch"])
        x_mini_batch = data_set_samples[rnd:rnd + param["mini-batch"]]

        for trace_index in range(param["mini-batch"]):
            x_train_shifted[trace_index] = x_mini_batch[trace_index]
            shift = random.randint(-5, 5)
            if shift > 0:
                x_train_shifted[trace_index][0:ns - shift] = x_mini_batch[trace_index][shift:ns]
                x_train_shifted[trace_index][ns - shift:ns] = x_mini_batch[trace_index][0:shift]
            else:
                x_train_shifted[trace_index][0:abs(shift)] = x_mini_batch[trace_index][ns - abs(shift):ns]
                x_train_shifted[trace_index][abs(shift):ns] = x_mini_batch[trace_index][0:ns - abs(shift)]

        if not mlp:
            x_train_shifted_reshaped = x_train_shifted.reshape((x_train_shifted.shape[0], x_train_shifted.shape[1], 1))

            yield x_train_shifted_reshaped, data_set_labels[rnd:rnd + param["mini-batch"]]
        yield x_train_shifted, data_set_labels[rnd:rnd + param["mini-batch"]]


def data_augmentation_noise(data_set_samples, data_set_labels, param, mlp=False):
    ns = param["number_of_samples"]

    while True:

        x_train_augmented = np.zeros((param["mini-batch"], ns))
        rnd = random.randint(0, len(data_set_samples) - param["mini-batch"])
        x_mini_batch = data_set_samples[rnd:rnd + param["mini-batch"]]

        for trace_index in range(param["mini-batch"]):
            noise = np.random.normal(0, 0.01, ns)
            x_train_augmented[trace_index] = x_mini_batch[trace_index] + noise

        if not mlp:
            x_train_augmented_reshaped = x_train_augmented.reshape(
                (x_train_augmented.shape[0], x_train_augmented.shape[1], 1))

            yield x_train_augmented_reshaped, data_set_labels[rnd:rnd + param["mini-batch"]]
        yield x_train_augmented, data_set_labels[rnd:rnd + param["mini-batch"]]
