from commons.sca_database import ScaDatabase
from commons.sca_parameters import ScaParameters
from database.tables import KeyRankJSON, SuccessRateJSON, Metric, Ensemble
from plots.PlotlyPlots import PlotlyPlots
import numpy as np


class ScaViews:

    def __init__(self, analysis_id, plot_color):
        self.db = ScaDatabase('scripts_aes/database.sqlite')
        self.analysis_id = analysis_id
        self.plot_color = plot_color

    def metric_plots(self):

        plotly_plots = PlotlyPlots()

        all_metric_plots = []

        accuracy_plots = []
        recall_plots = []
        loss_plots = []
        f1_plots = []
        ity_plots = []

        sca_parameters = ScaParameters()
        sca_metrics = sca_parameters.get_all_metrics()

        for metric in sca_metrics:
            metric_values = self.db.select_values_from_metric(Metric, metric, self.analysis_id)
            if len(metric_values) > 0:
                for metric_value in metric_values:
                    if "accuracy" in metric:
                        accuracy_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name=metric))
                    if "recall" in metric:
                        recall_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name=metric))
                    if "loss" in metric:
                        loss_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name=metric))
                    if "f1" in metric:
                        f1_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name=metric))

        all_metric_plots.append({
            "title": "Accuracy",
            "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Accuracy", self.plot_color),
            "plots": accuracy_plots
        })

        if len(recall_plots) > 0:
            all_metric_plots.append({
                "title": "Recall",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Recall", self.plot_color),
                "plots": recall_plots
            })

        all_metric_plots.append({
            "title": "Loss",
            "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Loss", self.plot_color),
            "plots": loss_plots
        })

        if len(f1_plots) > 0:
            all_metric_plots.append({
                "title": "F1",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "F1", self.plot_color),
                "plots": f1_plots
            })

        metric_values_ity = self.db.select_values_from_metric(Metric, "val_ity", self.analysis_id)
        if len(metric_values_ity) > 0:
            for metric_value in metric_values_ity:
                ity_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name="I(T,Y)"))

            all_metric_plots.append({
                "title": "Mutual Information Output Layer - I(T,Y)",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "I(T,Y)", self.plot_color),
                "plots": ity_plots
            })

        metric_values_ity = self.db.select_values_from_metric(Metric, "val_ity_ensembles", self.analysis_id)
        if len(metric_values_ity) > 0:
            for metric_value in metric_values_ity:
                ity_plots.append(plotly_plots.create_line_plot(y=metric_value['values'], line_name="val_ity_ensembles"))

            metric_values_ity = self.db.select_values_from_metric(Metric, "val_ity_ensembles_best_models",
                                                                  self.analysis_id)
            for metric_value in metric_values_ity:
                ity_plots.append(
                    plotly_plots.create_line_plot(y=metric_value['values'], line_name="val_ity_ensembles_best_models"))

            all_metric_plots.append({
                "title": "Mutual Information Output Layer - I(T,Y)",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "I(T,Y)", self.plot_color),
                "plots": ity_plots
            })

        return all_metric_plots

    def key_rank_plots(self):

        plotly_plots = PlotlyPlots()

        all_key_rank_plots = []
        key_rank_plots = []

        key_rank_all_key_bytes = self.db.select_values_from_analysis_json(KeyRankJSON, self.analysis_id)
        for key_rank_key_byte in key_rank_all_key_bytes:
            key_rank_plots_metrics = []
            for key_rank in key_rank_key_byte:
                key_rank_plots_metrics.append(
                    plotly_plots.create_line_plot(y=key_rank['values'], line_name="key byte " + str(
                        key_rank['key_byte']) + " " + key_rank['metric']))
            key_rank_plots.append(key_rank_plots_metrics)

        all_key_rank_plots.append({
            "title": "Key Rank (or Guessing Entropy)",
            "layout_plotly": plotly_plots.get_plotly_layout("Traces", "Guessing Entropy", self.plot_color),
            "plots": key_rank_plots
        })

        return all_key_rank_plots

    def success_rate_plots(self):

        plotly_plots = PlotlyPlots()

        all_success_rate_plots = []
        success_rate_plots = []

        success_rate_all_key_bytes = self.db.select_values_from_analysis_json(SuccessRateJSON, self.analysis_id)
        for success_rate_key_byte in success_rate_all_key_bytes:
            success_rate_plots_metrics = []
            for success_rate in success_rate_key_byte:
                success_rate_plots_metrics.append(
                    plotly_plots.create_line_plot(y=success_rate['values'], line_name="key byte " + str(
                        success_rate['key_byte']) + " " + success_rate['metric']))
            success_rate_plots.append(success_rate_plots_metrics)

        all_success_rate_plots.append({
            "title": "Success Rate",
            "layout_plotly": plotly_plots.get_plotly_layout("Traces", "Success Rate", self.plot_color),
            "plots": success_rate_plots
        })

        return all_success_rate_plots

    def ensemble_plots(self):

        plotly_plots = PlotlyPlots()

        all_ensemble_plots = []
        ensemble_plots = []

        ensemble_all_key_bytes = self.db.select_values_from_ensemble_json(Ensemble, self.analysis_id)
        for ensemble_all_key_byte in ensemble_all_key_bytes:
            ensemble_plots_metrics = []
            trace_set = []
            for ensemble in ensemble_all_key_byte:
                trace_set.append(ensemble['final_key_ranks'])
                ensemble_plots_metrics.append(
                    plotly_plots.create_hist_plot(x=ensemble['final_key_ranks'], line_name="key byte " + str(
                        ensemble['key_byte']) + " " + ensemble['metric']))
            ensemble_plots.append(ensemble_plots_metrics)

        all_ensemble_plots.append({
            "title": "Final Key Ranks",
            "layout_plotly": plotly_plots.get_layout_density("Final Key Ranks", "Density", self.plot_color),
            "plots": ensemble_plots
        })

        return all_ensemble_plots
