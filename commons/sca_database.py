from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker
import json


class ScaDatabase:

    def __init__(self, database_name):
        self.engine = create_engine('sqlite:///' + str(database_name), echo=False)
        self.metadata = MetaData(self.engine)
        self.session = sessionmaker(bind=self.engine)()

    def insert(self, row):
        self.session.add(row)
        self.session.commit()
        return row.id

    def select_all(self, table_class):
        return self.session.query(table_class).all()

    def select_from_analysis(self, table_class, analysis_id):
        return self.session.query(table_class).filter_by(analysis_id=analysis_id).all()[0]

    def select_analysis(self, table_class, analysis_id):
        return self.session.query(table_class).filter_by(id=analysis_id).all()[0]

    def select_values_from_analysis_json(self, table_class, analysis_id):
        key_byte_rows = self.session.query(table_class.key_byte).filter_by(analysis_id=analysis_id).distinct().all()
        key_bytes = [value for value, in key_byte_rows]
        values_all_key_bytes = []
        for key_byte in key_bytes:
            rows = self.session.query(table_class).filter_by(analysis_id=analysis_id, key_byte=key_byte).all()
            values_key_byte = []
            for row in rows:
                values_as_array = json.loads(row.values)
                values_list = []
                for index, value in values_as_array.items():
                    values_list.append(values_as_array[str(index)])

                values_key_byte.append({
                    "key_byte": key_byte,
                    "values": values_list,
                    "metric": row.metric
                })
            values_all_key_bytes.append(values_key_byte)
        return values_all_key_bytes

    def select_values_from_metric(self, table_class, metric, analysis_id):
        key_byte_rows = self.session.query(table_class.key_byte).filter_by(analysis_id=analysis_id,
                                                                           metric=metric).distinct().all()
        key_bytes = [value for value, in key_byte_rows]
        values_all_key_bytes = []
        for key_byte in key_bytes:
            rows = self.session.query(table_class.value).filter_by(analysis_id=analysis_id,
                                                                   key_byte=key_byte).filter_by(metric=metric).all()
            values = [value for value, in rows]
            values_all_key_bytes.append({
                "key_byte": key_byte,
                "values": values
            })
        return values_all_key_bytes

    def select_final_key_rank_json_from_analysis(self, table_class, analysis_id):
        key_byte_rows = self.session.query(table_class.key_byte).filter_by(analysis_id=analysis_id).distinct().all()
        key_bytes = [value for value, in key_byte_rows]
        values_all_key_bytes = []
        for key_byte in key_bytes:
            rows = self.session.query(table_class).filter_by(analysis_id=analysis_id, key_byte=key_byte).all()
            values_key_byte = []
            for row in rows:
                values_as_array = json.loads(row.values)
                values_list = []
                for index, value in values_as_array.items():
                    values_list.append(values_as_array[str(index)])

                values_key_byte.append({
                    "key_byte": key_byte,
                    "key_rank": int(values_list[len(values_list) - 1]),
                    "metric": row.metric
                })
            values_all_key_bytes.append(values_key_byte)
        return values_all_key_bytes

    def select_final_success_rate_from_analysis(self, table_class, analysis_id):
        key_byte_rows = self.session.query(table_class.key_byte).filter_by(analysis_id=analysis_id).distinct().all()
        key_bytes = [value for value, in key_byte_rows]
        values_all_key_bytes = []
        for key_byte in key_bytes:
            rows = self.session.query(table_class).filter_by(analysis_id=analysis_id, key_byte=key_byte).all()
            values_key_byte = []
            for row in rows:
                values_as_array = json.loads(row.values)
                values_list = []
                for index, value in values_as_array.items():
                    values_list.append(values_as_array[str(index)])

                values_key_byte.append({
                    "key_byte": key_byte,
                    "success_rate": round(values_list[len(values_list) - 1] * 100, 2),
                    "metric": row.metric
                })
            values_all_key_bytes.append(values_key_byte)
        return values_all_key_bytes

    def select_values_from_ensemble_json(self, table_class, analysis_id):
        key_byte_rows = self.session.query(table_class.key_byte).filter_by(analysis_id=analysis_id).distinct().all()
        key_bytes = [value for value, in key_byte_rows]
        values_all_key_bytes = []
        for key_byte in key_bytes:
            rows = self.session.query(table_class).filter_by(analysis_id=analysis_id, key_byte=key_byte).all()
            values_key_byte = []
            for row in rows:
                values_as_array = json.loads(row.final_key_ranks)
                values_list = []
                for index, value in values_as_array.items():
                    values_list.append(values_as_array[str(index)])

                values_key_byte.append({
                    "key_byte": key_byte,
                    "final_key_ranks": values_list,
                    "metric": row.metric
                })
            values_all_key_bytes.append(values_key_byte)
        return values_all_key_bytes
