import os


class ScaKerasModels:

    def __init__(self):
        pass

    def keras_model_as_string(self, keras_method_name):

        dir_name = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = os.path.join(dir_name, 'deep_learning_models')
        filename = file_path + "\\deeplearning.py"

        file_contents = ""

        file = open(filename, 'r')
        lines = file.readlines()
        print_line = False
        for line in lines:

            if keras_method_name + "(" in line:
                print_line = True

            if print_line:
                file_contents += line

                if "return" in line:
                    print_line = False

        return file_contents
