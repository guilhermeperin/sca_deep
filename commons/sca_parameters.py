class ScaParameters:

    def __init__(self):
        self.trace_set_list = []

    def get_trace_set(self, trace_set_name):
        trace_list = self.get_trace_set_list()
        return trace_list[trace_set_name]

    def get_basic_metrics(self):
        return ["accuracy", "val_accuracy", "test_accuracy", "recall", "val_recall", "loss", "val_loss", "f1", "val_f1"]

    def get_all_metrics(self):
        return ["accuracy", "val_accuracy", "test_accuracy", "accuracy_ensembles", "accuracy_ensembles_best_models",
                "val_accuracy_ensembles", "val_accuracy_ensembles_best_models",
                "recall", "val_recall", "recall_ensembles", "recall_ensembles_best_models",
                "val_recall_ensembles", "val_recall_ensembles_best_models",
                "loss", "val_loss", "loss_ensembles", "loss_ensembles_best_models",
                "val_loss_ensembles", "val_loss_ensembles_best_models",
                "f1", "val_f1", "f1_ensembles", "f1_ensembles_best_models",
                "val_f1_ensembles", "val_f1_ensembles_best_models"]

    def get_trace_set_list(self):
        parameters_ascad = {
            "name": "ascad_fixed_key",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 700,
            "n_train": 48000,
            "n_validation": 2000,
            "n_test": 10000,
            "trs_file_training": "ASCAD_k3 + Normalized + Training.trs",
            "trs_file_test": "ASCAD_k3 + Normalized + Test.trs",
            "classes": 9,
            "good_key": 224,
            "number_of_key_hypothesis": 256,
            "epochs": 100,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_ascad_random_key = {
            "name": "ascad_random_key",
            "key": "00112233445566778899AABBCCDDEEFF",
            "key_offset": 16,
            "input_offset": 0,
            "data_length": 50,
            "first_sample": 0,
            "number_of_samples": 140,
            "n_train": 199000,
            "n_validation": 1000,
            "n_test": 1000,
            "trs_file_training": "ASCAD_variable_key + TraceNormalization + WinRes.trs",
            "trs_file_test": "ASCAD_fixed_key + TraceNormalization + WinRes.trs",
            "classes": 9,
            "good_key": 34,
            "number_of_key_hypothesis": 256,
            "epochs": 100,
            "mini-batch": 2000,
            "random_key": True
        }

        parameters_pinata_aes_sw = {
            "name": "pinata_aes_sw",
            "key": "CAFEBABEDEADBEEF0001020304050607",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 32,
            "first_sample": 800,
            "number_of_samples": 400,
            "n_train": 6000,
            "n_validation": 1000,
            "n_test": 1000,
            "trs_file_training": "AES128_DL_Training + Normalized.trs",
            "trs_file_test": "AES128_DL_Test + Normalized.trs",
            "classes": 9,
            "good_key": 202,
            "number_of_key_hypothesis": 256,
            "epochs": 20,
            "mini-batch": 200,
            "random_key": False
        }

        parameters_ches_ctf = {
            "name": "ches_ctf",
            "key": "2EEE5E799D72591C4F4C10D8287F397A",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 48,
            "first_sample": 0,
            "number_of_samples": 2200,
            "n_train": 43000,
            "n_validation": 2000,
            "n_test": 2000,  # 5000 available
            "trs_file_training": "ChesCTF_training_45k + TraceNormalization.trs",
            "trs_file_test": "ChesCTF_test_5k + TraceNormalization.trs",
            "classes": 9,
            "good_key": 46,
            "number_of_key_hypothesis": 256,
            "epochs": 100,
            "mini-batch": 900,
            "random_key": True
        }

        parameters_ches_ctf_random_key = {
            "name": "ches_ctf_random_key",
            "key": "175CF2997A8583413C77DFAC7E6C59D8",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 48,
            "first_sample": 0,
            "number_of_samples": 569,
            "n_train": 9000,
            "n_validation": 1000,
            "n_test": 2000,  # 10000 available
            "trs_file_training": "ChesCTF_random_keys_training + TraceNormalization + WinRes.trs",
            "trs_file_test": "ChesCTF_fixed_key_test + TraceNormalization + WinRes.trs",
            "classes": 9,
            "good_key": 23,
            "number_of_key_hypothesis": 256,
            "epochs": 20,
            "mini-batch": 400,
            "random_key": True
        }

        parameters_dpa_v4 = {
            "name": "dpa_v4",
            "key": "6cecc67f287d083deb8766f0738b36cf",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 2000,
            "n_train": 34000,
            "n_validation": 2000,
            "n_test": 2000,
            "trs_file_training": "dpa_contest_v4_keyByte0 + Training.trs",
            "trs_file_test": "dpa_contest_v4_keyByte0 + Test.trs",
            "classes": 9,
            "good_key": 108,
            "number_of_key_hypothesis": 256,
            "epochs": 40,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_k210_5gs_avg100 = {
            "name": "k210_5gs_avg100",
            "key": "5F07A28E25D4C0AD3E1E825DDBD077FB",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 48,
            "first_sample": 0,
            "number_of_samples": 250,
            "n_train": 990000,
            "n_validation": 10000,
            "n_test": 10000,  # 10000 available
            "trs_file_training": "k210 + Random Inputs (Random Keys) 1M traces Avg100 5GS + Trim.trs",
            "trs_file_test": "k210 + Random Inputs (5F07A28E25D4C0AD3E1E825DDBD077FB) 100k traces Avg100 5GS + Trim.trs",
            "classes": 9,
            "good_key": 95,
            "number_of_key_hypothesis": 256,
            "epochs": 5,
            "mini-batch": 4000,
            "random_key": True
        }

        parameters_k210_20gs_avg100 = {
            "name": "k210_20gs_avg100",
            "key": "5F07A28E25D4C0AD3E1E825DDBD077FB",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 48,
            "first_sample": 3000,
            "number_of_samples": 500,
            "n_train": 98000,
            "n_validation": 2000,
            "n_test": 20000,
            "trs_file_training": "k210 + Random Inputs (Random keys) 100k traces Avg100 20GS.trs",
            "trs_file_test": "k210 + Random Inputs (5F07A28E25D4C0AD3E1E825DDBD077FB) 20k traces Avg100 20GS.trs",
            "classes": 9,
            "good_key": 95,
            "number_of_key_hypothesis": 256,
            "epochs": 20,
            "mini-batch": 1000,
            "random_key": True
        }

        parameters_k210_20gs = {
            "name": "k210_20g",
            "key": "CAFEBABEDEADBEEF0001020304050607",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 48,
            "first_sample": 200,
            "number_of_samples": 200,
            "n_train": 1000000,
            "n_validation": 5000,
            "n_test": 100000,
            "trs_file_training": "K210 + Random Inputs (5F07A28E25D4C0AD3E1E825DDBD077FB) 7M traces 20GS.trs",
            "trs_file_test": "K210 + Random Inputs (CAFEBABEDEADBEEF0001020304050607) 1M traces 20GS.trs",
            "classes": 9,
            "good_key": 202,
            "number_of_key_hypothesis": 256,
            "epochs": 100,
            "mini-batch": 4000,
            "random_key": True
        }

        parameters_ecc = {
            "name": "ecc",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 1,
            "first_sample": 0,
            "number_of_samples": 550,
            "n_train": 25500,
            "n_validation": 255,
            "n_test": 255,
            "trs_file_training": "ECC_Training + TraceNormalization.trs",
            "trs_file_test": "ECC_Test + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 40,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha = {
            "name": "ecc_ha",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 8000,
            "n_train": 63240,
            "n_validation": 510,
            "n_test": 12750,
            "trs_file_training": "ecc_from_ha_training.trs",
            "trs_file_test": "ecc_from_ha_test.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 20,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha_winres = {
            "name": "ecc_ha_winres",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "trs_file_training": "ecc_from_ha_training + winres + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test + winres + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 100,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha_winres_no_opt = {
            "name": "ecc_ha_winres_no_opt",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "trs_file_training": "ecc_from_ha_training_no_opt + winres + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test_no_opt + winres + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 10,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha_winres_pointer = {
            "name": "ecc_ha_winres_pointer",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 1000,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "trs_file_training": "ecc_from_ha_training_no_opt_pointer + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test_no_opt_pointer + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 2,
            "mini-batch": 64,
            "random_key": False
        }

        self.trace_set_list = {
            "ascad_fixed_key": parameters_ascad,
            "ascad_random_key": parameters_ascad_random_key,
            "ches_ctf": parameters_ches_ctf,
            "ches_ctf_random_key": parameters_ches_ctf_random_key,
            "dpa_v4": parameters_dpa_v4,
            "pinata_aes_sw": parameters_pinata_aes_sw,
            "k210_5gs_avg100": parameters_k210_5gs_avg100,
            "k210_20gs_avg100": parameters_k210_20gs_avg100,
            "k210_20gs": parameters_k210_20gs,
            "ecc": parameters_ecc,
            "ecc_ha": parameters_ecc_ha,
            "ecc_ha_winres": parameters_ecc_ha_winres,
            "ecc_ha_winres_no_opt": parameters_ecc_ha_winres_no_opt,
            "ecc_ha_winres_pointer": parameters_ecc_ha_winres_pointer
        }

        return self.trace_set_list
