from keras import backend as backend
from keras.optimizers import SGD, Adam, Adadelta, Adagrad, RMSprop, Adamax, Nadam
from keras.layers import Flatten, Dropout, Dense, Input, Conv1D, MaxPooling1D, RNN, SimpleRNN, BatchNormalization, LSTM, \
    Embedding, CuDNNLSTM, AveragePooling1D
from keras import regularizers
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.losses import categorical_crossentropy
from keras.layers import GaussianNoise
from keras.layers.merge import concatenate
import random
import numpy as np
import tensorflow as tf
from keras import Sequential


class DeepLearningModel:

    def __init__(self):
        pass

    def recall(self, y_true, y_pred):
        true_positives = backend.sum(backend.round(backend.clip(y_true * y_pred, 0, 1)))
        possible_positives = backend.sum(backend.round(backend.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + backend.epsilon())
        return recall

    def precision(self, y_true, y_pred):
        true_positives = backend.sum(backend.round(backend.clip(y_true * y_pred, 0, 1)))
        predicted_positives = backend.sum(backend.round(backend.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + backend.epsilon())
        return precision

    def f1(self, y_true, y_pred):
        precision = self.precision(y_true, y_pred)
        recall = self.recall(y_true, y_pred)
        return 2 * ((precision * recall) / (precision + recall + backend.epsilon()))

    def basic_lstm(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = BatchNormalization()(trace_input)
        x = LSTM(units=50, activation="relu")(x)
        x = Dense(50, activation='relu', name='fc1', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(50, activation='relu', name='fc2', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        # optimizer = Adam(lr=0.0001)
        optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_rnn(self, classes, number_of_samples):
        from keras.models import Model

        trace_input = Input(shape=[number_of_samples])
        x = Embedding(input_dim=number_of_samples, output_dim=2048)(trace_input)
        x = SimpleRNN(units=256, activation="relu")(x)
        # x = SimpleRNN(units=64, activation="relu")(x)
        # x = SimpleRNN(units=64, activation="relu")(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)
        model = Model(trace_input, x, name='cnn')
        model.compile(loss='categorical_crossentropy', optimizer=Adadelta(), metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_cnn(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        # x = BatchNormalization()(trace_input)
        x = Conv1D(filters=16, kernel_size=4, strides=4, activation='relu', padding='valid', name='block1_conv1')(
            trace_input)
        # x = MaxPooling1D(pool_size=2, strides=2, padding='valid', name='block1_pool1')(x)
        x = Conv1D(filters=32, kernel_size=4, strides=4, activation='relu', padding='valid', name='block1_conv2')(x)
        # x = MaxPooling1D(pool_size=2, strides=2, padding='valid', name='block1_pool2')(x)
        x = Conv1D(filters=64, kernel_size=4, strides=4, activation='relu', padding='valid', name='block1_conv3')(x)
        # x = MaxPooling1D(pool_size=2, strides=2, padding='valid', name='block1_pool3')(x)
        x = Flatten(name='flatten')(x)
        # x = Dropout(0.5)(x)
        # x = GaussianNoise(0.2)(x)
        x = Dense(100, activation='relu', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(100, activation='relu', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        optimizer = Adam(lr=0.00001)
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_cnn_ches_ctf(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        # x = BatchNormalization()(trace_input)
        x = Conv1D(filters=16, kernel_size=4, strides=4, activation='relu', padding='valid', name='block1_conv1')(
            trace_input)
        x = Flatten(name='flatten')(x)
        # x = Dropout(0.5)(x)
        # x = GaussianNoise(0.2)(x)
        x = Dense(100, activation='relu', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(100, activation='relu', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        optimizer = Adam(lr=0.001)
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_cnn_ascad_random_keys(self, classes, number_of_samples):

        model = Sequential()
        model.add(Conv1D(filters=16, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.00088), metrics=['accuracy', self.recall])

        return model

    def basic_cnn_dpav4(self, classes, number_of_samples):

        model = Sequential()
        model.add(Conv1D(filters=16, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(400, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(400, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_cnn_random(self, classes, number_of_samples, activation, neurons, conv_layers, filters, kernel_size,
                         stride, layers, learning_rate):
        model = Sequential()
        for l_i in range(conv_layers):
            if l_i == 0:
                model.add(
                    Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation='relu', padding='valid',
                           input_shape=(number_of_samples, 1))
                )
            else:
                model.add(
                    Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation='relu', padding='valid')
                )

        model.add(Flatten())
        for l_i in range(layers):
            model.add(
                Dense(neurons, activation=activation, kernel_initializer='random_uniform', bias_initializer='zeros'))

        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy',
                      optimizer=Adam(learning_rate=learning_rate),
                      metrics=['accuracy', self.recall])

        return model

    def basic_cnn_random_ha(self, classes, number_of_samples):
        from keras.models import Model

        neurons = random.randint(10, 50)
        layers = random.randint(1, 2)
        filters = random.randint(10, 20)
        act_fcn = ['relu', 'tanh', 'selu', 'elu', 'sigmoid']
        act = act_fcn[random.randint(0, 2)]

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = Conv1D(filters=filters, kernel_size=10, strides=10, activation='relu', padding='valid',
                   name='block1_conv1')(
            trace_input)
        # x = MaxPooling1D(pool_size=1, strides=1, padding='valid', name='block1_pool1')(x)
        x = Flatten(name='flatten')(x)
        for l in range(layers):
            x = Dropout(0.5)(x)
            x = Dense(neurons, activation=act, name='fc' + str(l), kernel_initializer='random_uniform',
                      bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        lr = random.uniform(0.0001, 0.001)
        optimizer = Adam(lr=lr)
        # optimizer = SGD(lr=0.01, decay=0, momentum=0.9, nesterov=True)
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        return model

    def basic_cnn_mlp_random(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        act_fcn = ['relu', 'tanh', 'selu', 'elu', 'sigmoid']
        act = act_fcn[random.randint(0, 2)]
        neurons = random.randint(20, 100)
        layers = random.randint(1, 5)
        filters = random.randint(10, 20)
        nn_type = random.randint(0, 1)

        if nn_type == 0:
            x = Conv1D(filters=filters, kernel_size=10, strides=10, activation='relu', padding='valid',
                       name='block1_conv1')(
                trace_input)
            x = Flatten(name='flatten')(x)
        else:
            x = Flatten(name='flatten')(trace_input)

        for l in range(layers):
            x = Dense(neurons, activation=act, name='fc' + str(l), kernel_initializer='random_uniform',
                      bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='mlp_cnn')
        lr = random.uniform(0.000001, 0.001)
        optimizer = Adam(lr=lr)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

        model.summary()

        return model

    def basic_mlp_random(self, classes, number_of_samples, activation, neurons, layers, learning_rate):
        model = Sequential()
        for l_i in range(layers):
            if l_i == 0:
                model.add(
                    Dense(neurons, activation=activation, kernel_initializer='random_uniform', bias_initializer='zeros',
                          input_shape=(number_of_samples,)))
            else:
                model.add(
                    Dense(neurons, activation=activation, kernel_initializer='random_uniform',
                          bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=learning_rate),
                      metrics=['accuracy', self.recall])

        return model

    def basic_mlp(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(40, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dense(40, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def mlp(self, classes, number_of_samples):

        model = Sequential()
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def lstm(self, classes, number_of_samples):
        model = Sequential()
        # model.add(Embedding(input_dim=16, output_dim=10, input_length=number_of_samples))
        model.add(CuDNNLSTM(units=10, input_shape=(number_of_samples, 1)))
        # model.add(LSTM(units=10))
        # model.add(Flatten())
        model.add(BatchNormalization())
        model.add(Dense(1400, activation="relu"))
        # model.add(Dropout(0.3))
        model.add(Dense(1400, activation="relu"))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_ches_ctf(self, classes, number_of_samples):

        model = Sequential()
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        # model.add(Dropout(0.3268))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))

        model.summary()

        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_dpav4(self, classes, number_of_samples):

        model = Sequential()
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))

        model.summary()

        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_ixt_ity(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        # x = BatchNormalization()(x)
        x = Dense(200, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(160, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(80, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(40, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(20, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(10, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.00005)
        # optimizer = Adagrad()
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_mlp_ixt_ity_relu(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        # x = BatchNormalization()(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)

        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        # optimizer = Adagrad()
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_mlp_ches_ctf_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(362, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dropout(0.3268))
        model.add(Dense(362, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))

        model.summary()

        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_ascad_random_keys(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dense(1400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_ascad_random_keys_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros',
                        input_shape=(number_of_samples,)))
        model.add(Dropout(0.25))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy', self.recall])

        return model

    def basic_mlp_hw_aes_avg100_5gs_100k(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = BatchNormalization()(x)
        # x = Dropout(0.5)(x)
        x = Dense(1000, activation='tanh', name='fc1', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(1000, activation='tanh', name='fc2', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(1000, activation='tanh', name='fc3', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        # x = Dropout(0.5)(x)
        x = Dense(1000, activation='tanh', name='fc4', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        # optimizer = Adagrad()
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_mlp_hw_aes_avg100_20gs_100k(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = BatchNormalization()(x)
        x = Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dropout(0.2)(x)
        x = Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dropout(0.2)(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_mlp_hw_aes_20gs(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = BatchNormalization()(x)
        # x = Dropout(0.5)(x)
        x = Dense(1000, activation='tanh', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc3', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc4', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc5', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc6', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(1000, activation='tanh', name='fc7', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        # optimizer = Adagrad()
        # optimizer = Adadelta()
        # optimizer = SGD(lr=0.05, decay=0, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    ### CNN Best model
    def cnn_best(self, classes, number_of_samples):

        from keras.models import Model
        # From VGG16 design
        input_shape = (number_of_samples, 1)
        img_input = Input(shape=input_shape)
        x = BatchNormalization()(img_input)
        # Block 1
        x = Conv1D(64, 11, activation='relu', padding='same', name='block1_conv1')(x)
        x = AveragePooling1D(2, strides=2, name='block1_pool')(x)
        # Block 2
        x = Conv1D(128, 11, activation='relu', padding='same', name='block2_conv1')(x)
        x = AveragePooling1D(2, strides=2, name='block2_pool')(x)
        # Block 3
        x = Conv1D(256, 11, activation='relu', padding='same', name='block3_conv1')(x)
        x = AveragePooling1D(2, strides=2, name='block3_pool')(x)
        # Block 4
        x = Conv1D(512, 11, activation='relu', padding='same', name='block4_conv1')(x)
        x = AveragePooling1D(2, strides=2, name='block4_pool')(x)
        # Block 5
        x = Conv1D(512, 11, activation='relu', padding='same', name='block5_conv1')(x)
        x = AveragePooling1D(2, strides=2, name='block5_pool')(x)
        # Classification block
        x = Flatten(name='flatten')(x)
        x = Dense(4096, activation='relu', name='fc1')(x)
        x = Dense(4096, activation='relu', name='fc2')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        inputs = img_input
        # Create model.
        model = Model(inputs, x, name='cnn_best')
        optimizer = RMSprop(lr=0.00001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()
        return model

    def basic_mlp_multiple(self, classes, number_of_samples):

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        outputs = []

        for index_model in range(256):
            x = Flatten(name='flatten_' + str(index_model))(trace_input)
            x = Dense(10, activation='tanh', name='fc1_' + str(index_model), kernel_initializer='random_uniform',
                      bias_initializer='zeros')(x)
            x = Dense(10, activation='tanh', name='fc2_' + str(index_model), kernel_initializer='random_uniform',
                      bias_initializer='zeros')(x)
            x = Dense(classes, activation='softmax')(x)

            outputs.append(x)

        from keras.models import Model
        model = Model(inputs=trace_input, outputs=outputs, name='mlp')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_ae(self, number_of_samples):
        from keras.models import Model
        from keras.layers import Reshape

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = Flatten(name='flatten')(trace_input)
        x = Dense(units=200, activation='relu')(x)
        x = Dense(units=100, activation='relu')(x)
        encoded = Dense(units=50, activation='relu')(x)

        x = Dense(units=100, activation='relu')(encoded)
        x = Dense(units=200, activation='relu')(x)
        x = Dense(units=number_of_samples, activation='relu')(x)
        decoded = Reshape((number_of_samples, 1))(x)

        auto_encoder = Model(trace_input, decoded)
        # optimizer = Adam(lr=0.01)
        auto_encoder.summary()
        auto_encoder.compile(optimizer='adam', loss='mse', metrics=['accuracy'])

        return auto_encoder

    def basic_mlp_param(self, classes, number_of_samples, layers, neurons, act, lr):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        act_fcn = ['relu', 'tanh', 'selu', 'elu', 'sigmoid']
        act = act_fcn[random.randint(0, 2)]
        neurons = random.randint(20, 100)
        layers = random.randint(1, 5)

        x = Flatten(name='flatten')(trace_input)
        for l in range(layers):
            x = Dense(neurons, activation=act, name='fc' + str(l), kernel_initializer='random_uniform',
                      bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='mlp')

        lr = lr
        optimizer = Adam(lr=lr)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall])

        model.summary()

        return model

    def basic_cnn2D(self, number_of_classes, input_shape):
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(number_of_classes, activation='softmax'))

        model.compile(loss=categorical_crossentropy, optimizer=Adadelta(),
                      metrics=['accuracy', self.f1_m, self.precision_m, self.recall])
        return model
