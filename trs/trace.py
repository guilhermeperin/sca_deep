import numpy as np
import trsfile
import random


class Trace:

    def __init__(self, filename, first_sample, number_of_samples, number_of_traces, data_length):
        self.filename = filename
        self.first_sample = first_sample
        self.number_of_samples = number_of_samples
        self.number_of_traces = number_of_traces
        self.data_length = data_length
        self.tracePath = ""

    def set_trace_directory_path(self, directory_path):
        self.tracePath = directory_path

    def open_trace(self, random_order=False):
        samples = np.empty(shape=(self.number_of_traces, self.number_of_samples), dtype="float32")
        data = []

        with trsfile.open(self.tracePath + self.filename) as trs_file:
            for i, trace in enumerate(trs_file[0:self.number_of_traces]):
                samples[i, :] = trs_file[i][self.first_sample:self.first_sample + self.number_of_samples]
                data.append(np.frombuffer(trace.data, dtype=np.uint8, count=self.data_length))

        if random_order:
            random_samples = np.empty(shape=(self.number_of_traces, self.number_of_samples), dtype="float32")
            random_data = []
            random_array = [value for value in range(0, self.number_of_traces)]
            random.shuffle(random_array)

            for i in range(0, self.number_of_traces):
                random_samples[i, :] = samples[random_array[i], :]
                random_data.append(data[random_array[i]])

            return random_samples, random_data
        else:
            return samples, data
