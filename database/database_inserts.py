from commons.sca_database import ScaDatabase
from database.tables import KeyRankJSON, SuccessRateJSON, Metric, Ensemble
from database.tables import Analysis, NeuralNetwork, LeakageModel, HyperParameter
import database.tables as tables


class DatabaseInserts:

    def __init__(self, database_name, dataset, script, elapsed_time, model_fixed=True, multiple_trainings=False):
        self.db = ScaDatabase(database_name)
        tables.base().metadata.create_all(self.db.engine)
        new_insert = Analysis(script=script, dataset=dataset, elapsed_time=elapsed_time, model_fixed=model_fixed,
                              multiple_trainings=multiple_trainings)
        self.analysis_id = self.db.insert(new_insert)

    def update_elapsed_time_analysis(self, elapsed_time):
        self.db.session.query(Analysis).filter(Analysis.id == self.analysis_id).update({"elapsed_time": elapsed_time})
        self.db.session.commit()

    def save_hyper_parameters(self, hyper_parameters):
        new_insert = HyperParameter(hyper_parameters=hyper_parameters, analysis_id=self.analysis_id)
        return self.db.insert(new_insert)

    def save_neural_network(self, description, model_name):
        new_insert = NeuralNetwork(model_name=model_name, description=description, analysis_id=self.analysis_id)
        return self.db.insert(new_insert)

    def save_leakage_model(self, leakage_model):
        new_insert = LeakageModel(leakage_model=leakage_model, analysis_id=self.analysis_id)
        return self.db.insert(new_insert)

    def save_metric(self, data, key_byte, metric):
        for value in data:
            new_insert = Metric(value=value, key_byte=key_byte, metric=metric, analysis_id=self.analysis_id)
            self.db.insert(new_insert)

    def save_key_rank_json(self, values, key_byte, metric):
        new_insert = KeyRankJSON(values=values, key_byte=key_byte, metric=metric, analysis_id=self.analysis_id)
        self.db.insert(new_insert)

    def save_success_rate_json(self, values, key_byte, metric):
        new_insert = SuccessRateJSON(values=values, key_byte=key_byte, metric=metric, analysis_id=self.analysis_id)
        self.db.insert(new_insert)

    def save_ensemble(self, final_key_ranks, key_byte, metric):
        new_insert = Ensemble(final_key_ranks=final_key_ranks, key_byte=key_byte, metric=metric,
                              analysis_id=self.analysis_id)
        self.db.insert(new_insert)
